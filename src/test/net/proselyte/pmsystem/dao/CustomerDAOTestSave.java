package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Alexey on 12/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerDAOTestSave {
    private Customer customer;
    private JdbcCustomerDAOImpl jdbcCustomerDAOImpl;

    @Before
    public void setUp() throws Exception {
        customer = mock(Customer.class);
        jdbcCustomerDAOImpl = mock(JdbcCustomerDAOImpl.class);
    }

    @Test
    public void saveTest() throws Exception {
        stubVoid(jdbcCustomerDAOImpl).toReturn().on().save(customer);
        jdbcCustomerDAOImpl.save(customer);
    }

}