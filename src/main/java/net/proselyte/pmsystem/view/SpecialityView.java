package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.dao.jdbc.JdbcSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

/**
 * Created by Raketa on 01.01.2017.
 */
public class SpecialityView {

  public void specialityView() throws IOException {
    Speciality speciality = new Speciality();
    JdbcSpecialityDAOImpl jdbcSpecialityDAO = new JdbcSpecialityDAOImpl();
    Long id;
    String name;

    writeMessage("" +
            "0 - Вывести все специальности \n" +
            "1 - Добавить специальность \n" +
            "2 - Удалить специальность \n" +
            "3 - Изменить данные о специальности \n" +
            "4 - Найти специальность по ID \n" +
            "5 - Выход в главное меню\n");


    int choice = readInt();

    switch (choice) {
      case 0:
        Set<Speciality> specialities = jdbcSpecialityDAO.getAll();
        specialities.forEach(System.out::println);
        break;
      case 1:
        writeMessage("Введите название специальности \n");
        name = readString();
        writeMessage("Введите id специальности \n");
        id = readLong();
        writeMessage("\n Специальность создана \n");

        speciality.setId(id);
        speciality.setName(name);

        jdbcSpecialityDAO.save(speciality);
        break;

      case 2:
        writeMessage("\n Введите id специальности \n");
        id = readLong();
        jdbcSpecialityDAO.remove(jdbcSpecialityDAO.getById(id));
        writeMessage("\n Специальность удалена \n");
        break;

      case 3:
        writeMessage("\n Введите id специальности \n");
        id = readLong();
        writeMessage("\n Введите новое название специальности \n");
        name = readString();

        speciality.setId(id);
        speciality.setName(name);

        jdbcSpecialityDAO.update(speciality);
        System.out.printf("\n Данные о специальности изменены \n");
        break;

      case 4:
        writeMessage("\n Введите id специальности \n");
        id = readLong();
        jdbcSpecialityDAO.getById(id);
        System.out.println(jdbcSpecialityDAO.getById(id).toString());
        break;
      case 5:
        writeMessage("\n Переходим в главное меню...\n");
        ConsoleHelper consoleHelper = new ConsoleHelper();
        consoleHelper.consoleHelp();
        break;
      default:
        break;
    }
    specialityView();
  }

  public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

  private static void writeMessage(String message) {
    System.out.println(message);
  }

  private static String readString() throws IOException {
    return bufferedReader.readLine();
  }

  private static int readInt() throws IOException {
    int number = 0;
    try {
      number = Integer.parseInt(bufferedReader.readLine());
    } catch (NumberFormatException e) {
      writeMessage("данные введены некорректно. Проверьте");
      readInt();
    }
    return number;
  }

  private static Long readLong() throws IOException {
    Long number = null;
    try {
      number = Long.parseLong(bufferedReader.readLine());
    } catch (NumberFormatException e) {
      writeMessage("данные введены некорректно. Проверьте");
      readLong();
    }
    return number;
  }
}
