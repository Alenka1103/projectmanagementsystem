
package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.ProjectController;
import net.proselyte.pmsystem.model.Project;

import java.io.IOException;

import static net.proselyte.pmsystem.view.CustomerView.*;

/**
 * Class that contains method to output data connected with {@link Project}
 *
 * @author Kyryl Potapenko
 */
public class ProjectView {
    ProjectController projectController;
    Long id;
    String nameProj;
    String descrProj;
    Long idTeam;
    Long idDoc;
    Long idCompany;


    public ProjectView() {
        projectController = new ProjectController();
    }

    public void projectView() throws IOException {
        Project project = new Project();
        writeMessage("\n * * * Проекты * * *" + "\n" +
                "1 - Добавить | 2 - Удалить | 3 - Изменить | 4 - Найти по ID  | 5 - Показать все проекты  | 6 - Выход в главное меню");

        int commandUser = readInt();

        switch (commandUser) {
            case 1:

                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please Id number of project: \n");
                id = readLong();
                writeMessage("Enter please name project: \n ");
                nameProj = readString();
                writeMessage("Enter please description project: \n");
                descrProj = readString();
                writeMessage("Please enter id of team: \n");
                idTeam = readLong();
                writeMessage("Please enter id of document: \n");
                idDoc = readLong();
                writeMessage("Please enter id of Company: \n");
                idCompany = readLong();
                saveProject(id, nameProj, descrProj, idTeam, idDoc, idCompany);
                projectView();
                break;
            case 2:
                writeMessage("Enter please ID project to delete: \n");
                id = readLong();
                deleteProject(id);
                projectView();
                break;
            case 3:
                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please name project: \n");
                nameProj = readString();
                writeMessage("enter  description: \n");
                descrProj = readString();
                writeMessage("Please enter id of team: \n");
                idTeam = readLong();
                writeMessage("Please enter id of document: \n");
                idDoc = readLong();
                writeMessage("Please enter id of Company: \n");
                idCompany = readLong();
                writeMessage("enter ID project: \n");
                id = readLong();
                updateProject(nameProj, descrProj, idTeam, idDoc, idCompany, id);
                projectView();
                break;
            case 4:
                writeMessage("Enter plese ID project: \n");
                id = readLong();
                searchProject(id);
                projectView();
                break;
            case 5:
                writeMessage("All projects:\n");
                projectController.showAllprojects();
                projectView();
                break;
            case 6:
                writeMessage("Exiting to main menu \n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;

            default:
                break;

        }

    }

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public void saveProject(Long id, String name, String description, Long teamId, Long documentId, Long CompanyId) {
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setTeamId(teamId);
        project.setDocumentId(documentId);
        project.setCompanyId(CompanyId);
        projectController.save(project);
        System.out.println("ID" + project.getId() + ", Name " + project.getName() + " Description: " + project.getDescription()
                + ", Team ID " + project.getTeamId() + ", Document ID " + project.getDocumentId() + ", companies ID " + project.getCompanyId() +
                ", is succefullt added.");
    }

    public void deleteProject(Long id) {
        Project project = projectController.getById(id);
        projectController.remove(project);
        System.out.println("You delete project: " + project.getId());
    }

    public void updateProject(String name, String description, Long teamId, Long documentId, Long CompanyId, Long id) {
        Project project = projectController.getById(id);
        project.setName(name);
        project.setDescription(description);
        project.setTeamId(teamId);
        project.setDocumentId(documentId);
        project.setCompanyId(CompanyId);
        projectController.update(project);
        System.out.println("ID" + project.getId() + ", Name " + project.getName() + " Description: " + project.getDescription()
                + ", Team ID " + project.getTeamId() + ", Document ID " + project.getDocumentId() + ", companies ID " + project.getCompanyId() +
                ", is succefullt update.");
    }

    public void searchProject(Long id) {
        Project project = projectController.getById(id);
        System.out.println(project);
    }

}

