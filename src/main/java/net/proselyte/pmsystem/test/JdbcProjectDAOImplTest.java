package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcProjectDAOImpl;
import net.proselyte.pmsystem.model.Project;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static net.proselyte.pmsystem.util.ConnectionUtil.connection;
import static net.proselyte.pmsystem.util.ConnectionUtil.preparedStatement;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link JdbcProjectDAOImpl}
 * @author Kyryl Potapenko
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcProjectDAOImplTest {

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmnt;
    @Mock
    ResultSet mockResultSet;

    Long Id;
    JdbcProjectDAOImpl jdbcProjectDAO = new JdbcProjectDAOImpl();
    Project project = new Project();

    public JdbcProjectDAOImplTest() {
        Id = 123L;
        project.setId(Id);
        project.setName("Name");
        project.setDescription("Desc");
        project.setTeamId(Id);
        project.setCompanyId(Id);
        project.setDocumentId(Id);
    }

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString(), anyInt())).thenReturn(mockPreparedStmnt);
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmnt);
        doNothing().when(mockPreparedStmnt).setLong(anyInt(), anyLong());
        doNothing().when(mockPreparedStmnt).setString(anyInt(), anyString());
        when(mockPreparedStmnt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        connection = mockConn;
        preparedStatement = mockPreparedStmnt;
    }

    @Test
    public void getByIdTest() throws SQLException {
        jdbcProjectDAO.getById(Id);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockConn, times(0)).commit();
    }
    @Test
    public void saveTest() throws SQLException {
        jdbcProjectDAO.save(project);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(4)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
    @Test
    public void updateTest() throws SQLException {
        jdbcProjectDAO.update(project);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(4)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
    @Test
    public void removeTest() throws SQLException {
        jdbcProjectDAO.remove(project);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
}