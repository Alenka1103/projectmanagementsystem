package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Raketa on 10.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcSpecialityDAOTestGetById {
  private JdbcSpecialityDAOImpl jdbcSpecialityDAO;
  private Speciality speciality;
  private Long id;

  @Before
  public void setUp() {
    speciality = mock(Speciality.class);
    jdbcSpecialityDAO = mock(JdbcSpecialityDAOImpl.class);
    id = new Long(speciality.getId());
  }

  @Test
  public void getByIdTest() throws Exception {
    when(jdbcSpecialityDAO.getById(id)).thenReturn(speciality);
    assertEquals(jdbcSpecialityDAO.getById(id), speciality);
  }
}
