-- Contains queries to fill database with data
INSERT INTO companies (id, name, description)
VALUES (151, 'Gigasoft', 'IT-outsorcing Company');

INSERT INTO companies (id, name, description)
VALUES (152, 'Softell', 'IT-outsorcing Company');

INSERT INTO companies (id, name, description)
VALUES (153, 'Softstream', 'IT-outsorcing Company');

INSERT INTO companies (id, name, description)
VALUES (154, 'OnlineSoft', 'Web-site constructing Company');

INSERT INTO customers (id, name, description)
VALUES (191, 'Citibank', 'International Bank');

INSERT INTO customers (id, name, description)
VALUES (192, 'AirFrance', 'AirCompany');

INSERT INTO customers (id, name, description)
VALUES (193, 'AlibabaGroup', 'Online-commerce website');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (131, 'Andrew', 'Bakeman', 31, 30000, 9, 'java');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (132, 'Ann', 'Jupan', 27, 10000, 5, 'testing');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (133, 'Mathew', 'Donaldson', 35, 50000, 15, 'cplusplus');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (134, 'Edward', 'Starjinski', 25, 11000, 5, 'testing');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (135, 'Jake', 'Holland', 38, 26000, 20, 'project-management');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (136, 'Nancy', 'Shwimmer', 26, 23000, 4, 'php');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (137, 'David', 'Monk', 32, 20000, 10, 'frontEnd');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (138, 'Trudy', 'Baseberg', 29, 35000, 8, 'javascript');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (139, 'Fedir', 'Ivanoff', 40, 48000, 21, 'cplusplus');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (140, 'Yuri', 'Mandelstamm', 42, 31000, 20, 'cplusplus');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (141, 'Harry', 'Jackson', 30, 35000, 7, 'project-management');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (142, 'John', 'Brown', 34, 41000, 13, 'java');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (143, 'Thomas', 'Roberts', 28, 18000, 5, 'testing');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (144, 'Nick', 'Cameron', 25, 23000, 8, 'php');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (145, 'Mandy', 'Miles', 39, 42000, 10, 'project-management');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (146, 'George', 'Wisconsin', 24, 10000, 2, 'frontEnd');

INSERT INTO developers (id, firstName, lastName, age, salary, yearsOfExperience, experience)
VALUES (147, 'William', 'Jods', 33, 18000, 7, 'frontEnd');

INSERT INTO documents (id, name, content)
VALUES (161, 'BBR', 'Business requirements for banking project');

INSERT INTO documents (id, name, content)
VALUES (162, 'MATS', 'Mapping for aircraft tracking system');

INSERT INTO documents (id, name, content)
VALUES (163, 'CCOPTR', 'Cryptocurrency online payments techreqs');

INSERT INTO skills (id, name)
VALUES (121, 'java');

INSERT INTO skills (id, name)
VALUES (122, 'cplusplus');

INSERT INTO skills (id, name)
VALUES (123, 'php');

INSERT INTO skills (id, name)
VALUES (124, 'javascript');

INSERT INTO skills (id, name)
VALUES (125, 'frontEnd');

INSERT INTO skills (id, name)
VALUES (126, 'project-management');

INSERT INTO skills (id, name)
VALUES (127, 'testing');

INSERT INTO specialities (id, name)
VALUES (111, 'javaDeveloper');

INSERT INTO specialities (id, name)
VALUES (112, 'cplusplus-developer');

INSERT INTO specialities (id, name)
VALUES (113, 'php-developer');

INSERT INTO specialities (id, name)
VALUES (114, 'javascript-developer');

INSERT INTO specialities (id, name)
VALUES (115, 'frontEnd-developer');

INSERT INTO specialities (id, name)
VALUES (116, 'project-manager');

INSERT INTO specialities (id, name)
VALUES (117, 'tester');

INSERT INTO teams (id, name)
VALUES (181, 'Banking-projectTeam');

INSERT INTO teams (id, name)
VALUES (182, 'Air-projectTeam');

INSERT INTO teams (id, name)
VALUES (183, 'Web-siteProjectTeam');

INSERT INTO сompany_customers (customersId, CompanyId)
VALUES (191, 153);

INSERT INTO сompany_customers (customersId, CompanyId)
VALUES (192, 151);

INSERT INTO сompany_customers (customersId, CompanyId)
VALUES (193, 154);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (131, 121);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (132, 122);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (133, 122);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (134, 127);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (135, 126);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (136, 123);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (137, 125);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (138, 124);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (139, 122);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (140, 122);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (141, 126);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (142, 121);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (143, 127);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (144, 123);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (145, 126);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (146, 125);

INSERT INTO developer_skills (developersId, skillsId)
VALUES (147, 125);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (131, 111);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (132, 117);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (133, 112);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (134, 117);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (135, 116);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (136, 113);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (137, 115);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (138, 114);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (141, 116);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (142, 111);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (143, 117);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (144, 113);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (145, 116);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (146, 115);

INSERT INTO developer_specialities (developersId, specialitiesId)
VALUES (147, 115);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 133);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 139);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 140);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 143);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 145);

INSERT INTO team_developers (teamId, developerId)
VALUES (181, 146);

INSERT INTO team_developers (teamId, developerId)
VALUES (182, 131);

INSERT INTO team_developers (teamId, developerId)
VALUES (182, 134);

INSERT INTO team_developers (teamId, developerId)
VALUES (182, 137);

INSERT INTO team_developers (teamId, developerId)
VALUES (182, 141);

INSERT INTO team_developers (teamId, developerId)
VALUES (182, 142);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 132);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 135);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 136);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 138);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 144);

INSERT INTO team_developers (teamId, developerId)
VALUES (183, 147);