package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.model.Speciality;

/**
 * Extension of {@link GenericDAO} interface for class {@link Speciality}.
 *
 * @author Eugene Suleimanov
 */
public interface SpecialtyDAO extends GenericDAO<Speciality, Long> {
}
