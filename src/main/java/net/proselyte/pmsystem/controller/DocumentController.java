package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;

/**
 * Created by Orange on 17.12.2016.
 */
public class DocumentController {

    JdbcDocumentDAOImpl jdbcDocumentDAO = new JdbcDocumentDAOImpl();

    public Document getDocumentById(long id) {return jdbcDocumentDAO.getById(id);
    }

    public void saveDocumentByName(Document document) {
        jdbcDocumentDAO.save(document);
    }

    public  void updateDocument (Document document){
        jdbcDocumentDAO.update(document);
    }

    public  void removeDocument (Document document){
        jdbcDocumentDAO.remove(document);
    }

}