package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;

/**
 * Created by Raketa on 17.12.2016.
 */
public class JdbcSpecialityDaoImplTest {




    public static void main(String[] args) {

    JdbcSpecialityDAOImpl jdbcSpecialtyDAO = new JdbcSpecialityDAOImpl();

    // Создадим объект Speciality
    Speciality speciality = new Speciality();
    speciality.setId(203L);
    speciality.setName("Test");

    System.out.println();
    System.out.println(" JdbcSpecialityDAOImpl getById 111L ");
    System.out.println(jdbcSpecialtyDAO.getById(111L));

    System.out.println("Creating instance of Speciality " + speciality);
    // Сохраним объект Speciality
    jdbcSpecialtyDAO.save(speciality);

    jdbcSpecialtyDAO.update(speciality);
    jdbcSpecialtyDAO.remove(speciality);
  }

}
