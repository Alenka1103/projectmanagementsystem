package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcCompanyDAOImpl;
import net.proselyte.pmsystem.model.Company;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Controller that handles requests from View connected with {@link Company}
 *
 * @author Kyryl Potapenko
 */
public class CompanyController implements Controller<Company, Long> {
    JdbcCompanyDAOImpl jdbcCompanyDAO = new JdbcCompanyDAOImpl();

    public Company getById(Long id) {
        return jdbcCompanyDAO.getById(id);
    }


    public void save(Company Company) {
        jdbcCompanyDAO.save(Company);
    }


    public void update(Company Company) {
        jdbcCompanyDAO.update(Company);
    }


    public void remove(Company Company) {
        jdbcCompanyDAO.remove(Company);
    }

    public Collection<Company> getAll() {
        Collection<Company> list = new ArrayList<Company>();
        return list = jdbcCompanyDAO.getAll();
    }

    public void showRelatedcustomers(Long id) { jdbcCompanyDAO.showRelatedcustomers(id);}

    public void showRelatedprojectsAndDev(Long id) { jdbcCompanyDAO.showRelatedprojectsAndDev(id);}
}
