package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcProjectDAOImpl;
import net.proselyte.pmsystem.model.Project;

/**
 * Controller that handles requests from View connected with {@link Project}
 *
 * @author Kyryl Potapenko
 */
public class ProjectController implements Controller<Project, Long> {
    JdbcProjectDAOImpl jdbcProjectDAO = new JdbcProjectDAOImpl();

    public Project getById(Long id) {
        return jdbcProjectDAO.getById(id);
    }

    public void save(Project project) {
        jdbcProjectDAO.save(project);
    }

    public void update(Project project) {
        jdbcProjectDAO.update(project);
    }

    public void remove(Project project) {
        jdbcProjectDAO.remove(project);
    }

    public void showAllprojects(){
        jdbcProjectDAO.showAllprojects();
    }


/*    public Collection<Project> showAllprojects() {
        Collection<Project> list = new ArrayList<Project>();
        return list = jdbcProjectDAO.showAllprojects();
    }
    */
}
