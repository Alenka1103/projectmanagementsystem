package net.proselyte.pmsystem.model;


/**
 * Simple JavaBean domain object that represents a Project.
 *
 * @author Eugene Suleimanov
 * @author Kyryl Potapenko
 */

public class Project extends NamedEntity {

    private String description;
    private Long CompanyId;
    private Long documentId;
    private Long teamId;

    public void setCompanyId(Long CompanyId) {
        this.CompanyId = CompanyId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Project(String name, String description) {
        super(name);
        this.description = description;
    }

    public Project() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCompanyId() {
        return CompanyId;
    }


    public Long getDocumentId() {
        return documentId;
    }


    public Long getTeamId() {
        return teamId;
    }


    @Override
    public String toString() {
        return "Project{" +
                "description='" + description + '\'' +
                ", CompanyId=" + CompanyId +
                ", documentId=" + documentId +
                ", teamId=" + teamId +
                '}';
    }
}
