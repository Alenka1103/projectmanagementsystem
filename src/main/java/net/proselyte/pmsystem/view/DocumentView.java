package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.DocumentController;
import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by proselyte on 21.12.16.
 */
public class DocumentView {
    private Scanner input = new Scanner(System.in);

    public void showMainMenu() throws IOException {
        System.out.println("    Document Menu ");
        System.out.println("1. Add document");
        System.out.println("2. Get document by ID");
        System.out.println("3. Delete document");
        System.out.println("4. Edit document");
        System.out.println("5. Return to Main Menu");

        int selection = input.nextInt();
        input.nextLine();

        switch (selection) {
            case 1:
                this.add();
                break;
            case 2:
                this.getAndPrintDocument();
                break;
            case 3:
                this.delete();
                break;
            case 4:
                this.update();
                break;
            case 5:
                this.exit();
                break;
            default:
                System.out.println("Invalid selection");
        }
    }

    private void getAndPrintDocument() {
        Document document = null;
        try {
            document = getDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(document.toString());
        System.out.println();
        System.out.println("************************");
        try {
            showMainMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exit() {
        System.out.println("We're about to main menu...");
        ConsoleHelper consoleHelper = new ConsoleHelper();
        try {
            consoleHelper.consoleHelp();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * it doesn't work in proper way
     * check update not existed ID
     */

    private void update() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Long id = null;
        String name = null;
        String content = null;
        try {
            id = getNumber();
            while(checkIfExist(id)){
                System.out.println("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        System.out.println("Enter document's name");
        try {
            name = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Enter content of the document");
        try {
            content = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Document document = new Document(id, name, content);
        DocumentController documentController = new DocumentController();
        documentController.updateDocument(document);

        System.out.println("Data successfully updated");
        try {
            showMainMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void delete() {
        Document document = null;
        try {
            document = getDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DocumentController documentController = new DocumentController();
        documentController.removeDocument(document);
        System.out.println("Data successfully deleted");
        try {
            showMainMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Document getDocument() {
        Long id = null;
        try {
            id = getNumber();
            while(!checkIfExist(id)){
                System.out.println("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Document document;
        DocumentController documentController = new DocumentController();
        return document = documentController.getDocumentById(id);
    }

    private void add() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Long id = null;
        String name = null;
        String content = null;
        try {
            id = getNumber();
            while(checkIfExist(id)){
                System.out.println("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        System.out.println("Enter document's name");
        try {
            name = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Enter content of the document");
        try {
            content = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Document document = new Document(id, name, content);
        DocumentController documentController = new DocumentController();
        documentController.saveDocumentByName(document);
        showMainMenu();
    }

    private Long getNumber(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter ID of the document");
        while (!scanner.hasNextLong()) {
            System.out.println("Invalid input\n Type the Long-type number:");
            scanner.next();
        }
        Long number = scanner.nextLong();

        return number;
    }

    private boolean checkIfExist(Long id){
        boolean result = false;
        JdbcDocumentDAOImpl jdbcDocumentDAO = new JdbcDocumentDAOImpl();
        Document document = jdbcDocumentDAO.getById(id);
        if(document != null){
            result = true;
        }
        return result;
    }
}
