package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;
import net.proselyte.pmsystem.model.Project;
import net.proselyte.pmsystem.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alexey on 12/18/2016.
 */
public class JdbcCustomerDAOImplTest {
    public static void main(String[] args){
        Set<Project> projects = new HashSet<>();
        projects.add(new Project());
        projects.add(new Project());
        Customer customer = new Customer((long) 7, "FirstName77", "SomeDescription77", projects);
        JdbcCustomerDAOImpl jdbcCustomerDAO = new JdbcCustomerDAOImpl();
        if (!checkIfIdExist(customer.getId())) {

            jdbcCustomerDAO.save(customer);
            System.out.println("Data saved successfully");
        } else {
            System.out.println("Enter other id");
        }

        for (long i = 1; checkIfIdExist(i); i++) {
            Customer customer1 = jdbcCustomerDAO.getById(i);
            System.out.println(customer1);
        }

        Customer customerToUpdate = new Customer((long)7,"sometext", "somedescription78", projects);
        jdbcCustomerDAO.update(customerToUpdate);

        for(long i = 1; checkIfIdExist(i); i++){
            Customer customer2 = jdbcCustomerDAO.getById(i);
            System.out.println(customer2);
        }

       jdbcCustomerDAO.remove(customerToUpdate);

        ArrayList<Customer> arrayList = (ArrayList<Customer>) getAllcustomers();
        for (Customer customer1 : arrayList) {
            System.out.println(customer1);
        }


    }

    private static Collection<Customer> getAllcustomers() {
        try {
            try(Connection connection = ConnectionUtil.connection) {
                try(Statement statement = connection.createStatement()){
                    Collection<Customer> list = new ArrayList<Customer>();
                    try(ResultSet resultSet = statement.executeQuery("SELECT * FROM customers")){
                        while(resultSet.next()){
                            Customer customer1 = new Customer();
                            customer1.setId(resultSet.getLong(1));
                            customer1.setName(resultSet.getString(2));
                            customer1.setDescription(resultSet.getString(3));
                            list.add(customer1);
                        }
                    }
                    return list;
                }

            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static boolean checkIfIdExist(Long id) {
        boolean result = false;
        JdbcCustomerDAOImpl jdbcCustomerDAO = new JdbcCustomerDAOImpl();
        Customer customer;
        customer = jdbcCustomerDAO.getById(id);
        if(customer != null){
            result = true;
        }
        return result;
    }


}