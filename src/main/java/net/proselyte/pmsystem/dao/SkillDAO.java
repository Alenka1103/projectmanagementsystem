package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.model.Skill;

/**
 * Created by Руслан on 18.12.2016.
 */
public interface SkillDAO extends GenericDAO<Skill, Long> {
}
