package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

import static net.proselyte.pmsystem.util.ConnectionUtil.connection;
import static net.proselyte.pmsystem.util.ConnectionUtil.preparedStatement;

/**
 * Created by Raketa on 10.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcSpecialityDaoImplMockitoTest {
  @Mock
  Connection mockConn;
  @Mock
  PreparedStatement mockPreparedStmnt;
  @Mock
  ResultSet mockResultSet;
  @Mock
  DataSource mockDataSource;

  Long Id;
  JdbcSpecialityDAOImpl jdbcSpecialityDAO = new JdbcSpecialityDAOImpl();
  Speciality speciality = new Speciality();

  public JdbcSpecialityDaoImplMockitoTest() {
    Id = 333L;
    speciality.setId(Id);
    speciality.setName("Mockito");
  }

  @Before
  public void setUp() throws SQLException {
    when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmnt);
    doNothing().when(mockPreparedStmnt).setLong(anyInt(), anyLong());
    doNothing().when(mockPreparedStmnt).setString(anyInt(), anyString());
    when(mockPreparedStmnt.executeQuery()).thenReturn(mockResultSet);
    when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
    connection = mockConn;
    preparedStatement = mockPreparedStmnt;
  }

  @Test
  public void getByIdTest() throws SQLException {
    jdbcSpecialityDAO.getById(Id);

    verify(mockConn, times(1)).prepareStatement(anyString());
    verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
    verify(mockPreparedStmnt, times(1)).executeQuery();
    verify(mockResultSet, times(1)).next();
    verify(mockPreparedStmnt, times(1)).close();
  }
  @Test
  public void saveTest() throws SQLException {
    jdbcSpecialityDAO.save(speciality);

    verify(mockConn, times(1)).prepareStatement(anyString());
    verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
    verify(mockPreparedStmnt, times(1)).setString(anyInt(), anyString());

    verify(mockPreparedStmnt, times(1)).execute();
    verify(mockPreparedStmnt, times(1)).close();
  }
  @Test
  public void updateTest() throws SQLException {
    jdbcSpecialityDAO.update(speciality);

    verify(mockConn, times(1)).prepareStatement(anyString());
    verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());
    verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());

    verify(mockPreparedStmnt, times(1)).executeUpdate();
    verify(mockPreparedStmnt, times(1)).close();
  }
  @Test
  public void removeTest() throws SQLException {
    jdbcSpecialityDAO.remove(speciality);

    verify(mockConn, times(1)).prepareStatement(anyString());
    verify(mockPreparedStmnt, times(1)).setString(anyInt(), anyString());
    verify(mockPreparedStmnt, times(1)).executeUpdate();
    verify(mockPreparedStmnt, times(1)).close();
  }

}
