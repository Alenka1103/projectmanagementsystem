package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.GenericDAO;
import net.proselyte.pmsystem.dao.ProjectDAO;
import net.proselyte.pmsystem.model.Project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static net.proselyte.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link GenericDAO} interface for class {@link Project}.
 *
 * @author Kyryl Potapenko
 */
public class JdbcProjectDAOImpl implements ProjectDAO {
    private Project project;
    private String SQLSearchById = "SELECT * FROM projects WHERE id = ?";
    private String SQLUpdate = "UPDATE projects SET name = ? , description =?, teamId=?, documentId=?, CompanyId=?  where id = ?";
    private String SQLDelete = "DELETE from projects where id = ?";
    private String SQLSave = "INSERT INTO projects VALUES (?,?,?,?,?,?)";

    public JdbcProjectDAOImpl() {
        try {
            getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void showAllprojects() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM projects");

            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String name = resultSet.getString(2);
                String desc = resultSet.getString(3);
                System.out.println(id + "   " + name + "   " + desc);
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }


    @Override
    public Project getById(Long id) {

        try {
            getConnection();
            preparedStatement = connection.prepareStatement(SQLSearchById);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            project = new Project();
            if (resultSet.next()) {
                project.setId(resultSet.getLong("id"));
                project.setName(resultSet.getString("name"));
                project.setDescription(resultSet.getString("description"));
                project.setTeamId(resultSet.getLong("teamId"));
                project.setDocumentId(resultSet.getLong("documentId"));
                project.setCompanyId(resultSet.getLong("CompanyId"));
            } else {
                throw new SQLException();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
        }
        return project;
    }

    @Override
    public void save(Project entity) {
        try {
            getConnection();
            preparedStatement = connection.prepareStatement(SQLSave);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getDescription());
            preparedStatement.setLong(4, entity.getTeamId());
            preparedStatement.setLong(5, entity.getDocumentId());
            preparedStatement.setLong(6, entity.getCompanyId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public void update(Project entity) {
        try {
            getConnection();
            preparedStatement = connection.prepareStatement(SQLUpdate);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setLong(3, entity.getTeamId());
            preparedStatement.setLong(4, entity.getDocumentId());
            preparedStatement.setLong(5, entity.getCompanyId());
            preparedStatement.setLong(6, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void remove(Project entity) {
        try {
            getConnection();
            preparedStatement = connection.prepareStatement(SQLDelete);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
        }

    }
}

