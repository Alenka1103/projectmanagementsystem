package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.SkillDAO;
import net.proselyte.pmsystem.model.Skill;
import net.proselyte.pmsystem.view.ConsoleHelper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import static net.proselyte.pmsystem.util.ConnectionUtil.*;

/**
 * @author Created by Руслан on 18.12.2016.
 * @author Kyryl Potapenko
 */
public class JdbcSkillDAOImpl implements SkillDAO {

    public final static String INSERT = "INSERT INTO skills VALUES (?,?)";
    public final static String GET_BY_ID = "SELECT * FROM skills WHERE ID = ?";
    public final static String UPDATE = "UPDATE skills SET  name = ? WHERE ID = ? ";
    public final static String DELETE = "DELETE FROM skills WHERE ID = ?";
    public final static String SHOWALL = "SELECT * FROM skills";
    private Skill skill;

    public JdbcSkillDAOImpl() {

    }

    @Override
    public Skill getById(Long id) {

        try {
            getConnection();
            preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            skill = new Skill();
            if (resultSet.next()) {
                skill.setId(resultSet.getLong(1));
                skill.setName(resultSet.getString(2));
            } else {
                throw new SQLException();
            }

        } catch (SQLException e) {

        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getStackTrace());
        } finally {
            closePreparedStatement();
        }
        return skill;
    }

    @Override
    public void save(Skill skill) {

        try {
            getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.setString(2, skill.getName());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getStackTrace());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getStackTrace());
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void update(Skill skill) {

        try {
            getConnection();

            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, skill.getName());
            preparedStatement.setLong(2, skill.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getStackTrace());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getStackTrace());
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void remove(Skill skill) {

        try {
            getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getStackTrace());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getStackTrace());
        } finally {
            closePreparedStatement();
        }

    }

    public void showAllBase() {
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOWALL);
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int numberOfColumns = resultSetMetaData.getColumnCount();
            for (int i = 1; i <= numberOfColumns; i++) {
                if (i > 1) System.out.print(",  ");
                String columnName = resultSetMetaData.getColumnName(i);
                System.out.print(columnName);
            }
            System.out.println();
            while (resultSet.next()) {
                for (int i = 1; i <= numberOfColumns; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(columnValue);
                }
                System.out.println("");
            }
            statement.close();
            ConsoleHelper consoleHelper = new ConsoleHelper();
            consoleHelper.consoleHelp();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
