package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcSkillDAOImpl;
import net.proselyte.pmsystem.model.Skill;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Руслан on 19.12.2016.
 */


    @RunWith(MockitoJUnitRunner.class)
    public class JdbcSkillDAOTestGetByID {
        private Skill skill;
        private JdbcSkillDAOImpl jdbcSkillDAOImpl;
        Long id;

        @Before
        public void setUp() throws Exception {
            skill = mock(Skill.class);
            jdbcSkillDAOImpl = mock(JdbcSkillDAOImpl.class);
            id = new Long(skill.getId());
        }

        @Test
        public void getByIdTest() throws Exception {
            when(jdbcSkillDAOImpl.getById(id)).thenReturn(skill);
            assertEquals(jdbcSkillDAOImpl.getById(id), skill);
        }

    }


