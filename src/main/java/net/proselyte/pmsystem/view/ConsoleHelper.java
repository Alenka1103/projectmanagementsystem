
package net.proselyte.pmsystem.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 * View class that contains methods for user-console interactions.
 *
 * @author Eugene Suleimanov
 * @author Kyryl Potapenko
 */
public class ConsoleHelper {

    private CompanyView CompanyView;
    private CustomerView customerView;
    private DeveloperView developerView;
    private ProjectView projectView;
    private SkillView skillView;
    private SpecialityView specialtyView;
    private TeamView teamView;

    public ConsoleHelper() {
        CompanyView = new CompanyView();
        customerView = new CustomerView();
        developerView = new DeveloperView();
        projectView = new ProjectView();
        skillView = new SkillView();
        specialtyView = new SpecialityView();
        teamView = new TeamView();
    }

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public void consoleHelp() throws IOException {
        System.out.println("Для осуществления операций CRUD выберите соответствующий компонент и нажмите ввод: \n 1 Customer \n 2 Company \n 3 Project \n 4 Team \n 5 Developer \n 6 Speciality \n 7 Skill \n 8 Exit");
        int readChoice = readInt();
        switch (readChoice) {
            case 1:
                customerView.customerView();
                break;
            case 2:
                CompanyView.execute();
                break;
            case 3:
                projectView.projectView();
                break;
            case 4:
                teamView.choice();
                break;
            case 5:
                developerView.execute();
                break;
            case 6:
                specialtyView.specialityView();
                break;
            case 7:
                skillView.skillView();
                break;
            case 8:
                System.out.println("Exiting....");
                System.exit(1);
            default:
                break;
        }
    }

    public static String readString() throws IOException {
        return bufferedReader.readLine();
    }

    public static int readInt() throws IOException {
        int number = 0;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Введены некорректные данные. Повторите ввод.");
            readInt();
        }
        return number;
    }

    public static Long readLong() throws IOException {
        Long number = null;
        try {
            number = Long.parseLong(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Введены некорректные данные. Повторите ввод.");
            readLong();
        }
        return number;
    }

    public static BigDecimal readBigDecimal() throws IOException {
        BigDecimal number = null;
        try {
            number = new BigDecimal(readLong());
        } catch (NumberFormatException e) {
            writeMessage("Введены некорректные данные. Повторите ввод.");
            readBigDecimal();
        }
        return number;
    }
}

