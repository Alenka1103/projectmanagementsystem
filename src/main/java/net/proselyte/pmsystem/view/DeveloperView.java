package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.DeveloperController;
import net.proselyte.pmsystem.dao.jdbc.JdbcDeveloperDAOImpl;
import net.proselyte.pmsystem.model.Developer;

import java.io.IOException;
import java.math.BigDecimal;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Developer}
 *
 * @author Kyryl Potapenko/
 * Updated by Iryna Seliutina on 14.01.2016.
 */
public class DeveloperView {
    public void execute() throws IOException {
        Developer developer = new Developer();
        DeveloperController developerController = new DeveloperController();
        JdbcDeveloperDAOImpl jdbcDeveloperDAO = new JdbcDeveloperDAOImpl();
        String data;
        Long id;
        Integer integer;
        BigDecimal salary;

        writeMessage("* * * Разработчики * * *" + "\n" +
                "1 - Добавить \n"+ "2 - Удалить \n"+ "3 - Изменить \n"+ "4 - Найти по ID \n"+ "5 - Показать всех разработчиков \n"+ "6 - Вернуться в главное меню \n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Укажите ID разработчика:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Укажите имя разработчика:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nУкажите фамилию разработчика:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nУкажите возраст разработчика:\n");
                integer = readInt();
                developer.setAge(integer);
                writeMessage("\nУкажите оклад разработчика:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nУкажите опыт работы разработчика:\n");
                integer = readInt();
                developer.setYearsOfExperience(integer);
                writeMessage("Укажите специализацию разработчика:\n");
                data = readString();
                developer.setExperience(data);
                writeMessage("\nРазработчик создан!\n");
                developerController.save(developer);
                System.out.println(developerController.getDeveloperById(id).toString());
                break;
            case 2:
                writeMessage("Укажите ID разработчика:\n");
                id = readLong();
                developerController.delete(developerController.getDeveloperById(id));
                writeMessage("\nРазработчик удален!\n");
                break;
            case 3:
                writeMessage("Укажите ID разработчика:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Укажите новое имя разработчика:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nУкажите новую фамилию разработчика:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nУкажите возраст разработчика:\n");
                integer = readInt();
                developer.setAge(integer);
                writeMessage("\nУкажите новый оклад разработчика:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nУкажите опыт работы разработчика:\n");
                integer = readInt();
                developer.setYearsOfExperience(integer);
                writeMessage("Укажите специализацию разработчика:\n");
                data = readString();
                developer.setExperience(data);
                developerController.update(developer);
                writeMessage("\nИзменения выполнены!\n");
                break;
            case 4:
                writeMessage("Укажите ID разработчика:\n");
                id = readLong();
                developerController.getDeveloperById(id);
                System.out.println(jdbcDeveloperDAO.getById(id).toString());
                break;
            case 5:
                developerController.showAllDevelopers(developer);
                break;
            case 6:
                writeMessage("Вернуться в главное меню:\n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();


            default:
                break;
        }
        execute();
    }
}