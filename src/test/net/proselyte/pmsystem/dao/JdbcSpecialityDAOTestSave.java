package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Raketa on 10.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcSpecialityDAOTestSave {
  private JdbcSpecialityDAOImpl jdbcSpecialityDAO;
  private Speciality speciality;


  @Before
  public void setUp() {
    speciality = mock(Speciality.class);
    jdbcSpecialityDAO = mock(JdbcSpecialityDAOImpl.class);
  }

  @Test
  public void removeTest() throws Exception {
    stubVoid(jdbcSpecialityDAO).toReturn().on().save(speciality);
    jdbcSpecialityDAO.save(speciality);
  }
}
