package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.SpecialtyDAO;
import net.proselyte.pmsystem.model.Speciality;
import net.proselyte.pmsystem.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import static net.proselyte.pmsystem.util.ConnectionUtil.connection;
import static net.proselyte.pmsystem.util.ConnectionUtil.getConnection;
import static net.proselyte.pmsystem.util.ConnectionUtil.statement;

/**
 * Created by Стрела on 10.12.2016.
 * Implement JDBC implementation of interface GenericDAO for class Speciality
 */
public class JdbcSpecialityDAOImpl implements SpecialtyDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcSpecialityDAOImpl.class);

    public JdbcSpecialityDAOImpl(){
        try{
            getConnection();
        } catch (SQLException e) {
            LOGGER.error("Connetion failed " + e);
        } catch (ClassNotFoundException e) {
            LOGGER.error("Driver not found " + e);
        }
    }

    @Override
    public Speciality getById(Long id) {
        String sql = "SELECT * FROM specialities WHERE ID = ?";
        try (PreparedStatement statement =connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return createSpecialty(resultSet);
            } else {
                LOGGER.error("Специальности с id " + id + " не существует в БД");
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
        // throw RE заменил на логгер и вписал return null
        return null;
    }

    public Set<Speciality> getAll() {
        Set<Speciality> specialities = new HashSet<>();
        String sql = "SELECT * FROM specialities";
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)){
            while (resultSet.next()){
                specialities.add(createSpecialty(resultSet));
            }
        }catch (SQLException e){
            LOGGER.error("Exception occurred while connection to DB", e);
        }
        return specialities;
    }

    @Override
    public void save(Speciality speciality) {
        String sql = "INSERT INTO specialities VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, speciality.getId());
            statement.setString(2, "'"+speciality.getName()+"'");
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
    }


    @Override
    public void update(Speciality speciality) {
        String sql = "UPDATE specialities SET name = ? WHERE NAME = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, speciality.getName());
            statement.setString(2, speciality.getName()); // может метод должен принимать два значения что изменить и на что изменить
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
    }

    @Override
    public void remove(Speciality speciality) {
        String sql = "DELETE FROM specialities WHERE name = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "'"+speciality.getName()+"'");
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
    }

    private Speciality createSpecialty(ResultSet resultSet) throws SQLException {
        Speciality speciality = new Speciality();
        speciality.setId(resultSet.getLong("ID"));
        speciality.setName(resultSet.getString("NAME"));
        return speciality;
    }
}

