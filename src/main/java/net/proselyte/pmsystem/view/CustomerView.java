package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.CustomerController;
import net.proselyte.pmsystem.dao.jdbc.JdbcCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by proselyte on 21.12.16.
 */
public class CustomerView {


    public void customerView() throws IOException {
        Customer customer = new Customer();
        CustomerController customerController = new CustomerController();
        Long id;
        String name;
        String description;

        writeMessage(" 1 - Добавить клиента \n 2 - Удалить клиента \n 3 - Изменить данные о клиенте \n 4 - Найти клиента по ID \n 5 - Вывести клиентов на экран \n 6 - Выйти в главное меню\n");

        int choice = readInt();

        switch (choice) {
            case 1:
                writeMessage("Наберите название клиента \n");
                name = readString();
                writeMessage("\n Наберите id клиента \n");
                id = readLong();
                writeMessage("\n Наберите описание клиента \n");
                description = readString();
                writeMessage("\n Клиент создан \n");

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.save(customer);
                break;

            case 2:
                writeMessage("\n Наберите id клиента \n");
                id = readLong();
                customerController.remove(customerController.getById(id));
                writeMessage("\n Клиент удален \n");
                break;

            case 3:
                writeMessage("\n Наберите id клиента \n");
                id = readLong();
                writeMessage("\n Наберите новое название клиента \n");
                name = readString();
                writeMessage("\n Наберите новое описание клиента \n");
                description = readString();

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.update(customer);
                System.out.println("\n Данные о клиенте изменены \n");
                break;

            case 4:
                writeMessage("\n Наберите id клиента \n");
                id = readLong();
                customerController.getById(id);
                System.out.println(customerController.getById(id).toString());
                break;

            case 5:
                writeMessage("Клиенская база: \n");
                customerController.showCustomer(customer);
                break;

            case 6:
                writeMessage("Переходим в главное меню...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }
        customerView();
    }

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() throws IOException {
        return bufferedReader.readLine();
    }

    public static int readInt() throws IOException {
        int number = 0;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Данные набраны некорректно. Повторите");
            readInt();
        }
        return number;
    }

    public static Long readLong() throws IOException {
        Long number = null;
        try {
            number = Long.parseLong(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Данные набраны некорректно. Повторите");
            readLong();
        }
        return number;

    }
}
