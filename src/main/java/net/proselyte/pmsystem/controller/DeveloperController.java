package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcDeveloperDAOImpl;
import net.proselyte.pmsystem.model.Developer;
import net.proselyte.pmsystem.model.Skill;
import net.proselyte.pmsystem.model.Speciality;

import java.util.Set;

/**
 * Created by Orange on 25.12.2016.
 *  Updated by Iryna Seliutina on 14.01.2016.
 */
public class DeveloperController {
        JdbcDeveloperDAOImpl jdbcDeveloperDAO = new JdbcDeveloperDAOImpl();

        public Developer getDeveloperById(Long id){
        return jdbcDeveloperDAO.getById(id);
        }

        public void save (Developer developer) {
            jdbcDeveloperDAO.save(developer);
        }

        public  void  update (Developer developer){
            jdbcDeveloperDAO.update(developer);
        }

        public void delete (Developer developer){
            jdbcDeveloperDAO.remove(developer);
        }

    public void showAllDevelopers (Developer developer) {
        jdbcDeveloperDAO.showAllDevelopers(developer);
    }

        public  void addSkillToDeveloperList (Skill skill){
            Developer developer = new Developer();
            Set<Skill> skills = developer.getSkills();
            skills.add(skill);
            developer.setSkills(skills);
        }

        public void addSpecialityToDeveloperList (Speciality speciality){
        Developer developer = new Developer();
        Set<Speciality> specialities = developer.getSpecialties();
        specialities.add(speciality);
        developer.setSpecialties(specialities);
        }
}
