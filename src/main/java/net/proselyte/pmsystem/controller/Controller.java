package net.proselyte.pmsystem.controller;
/**
 * Controller interface. Used as a base class for Controller classes.
 *
 * @author Kyryl Potapenko
 */
public interface Controller<T, ID> {

    T getById(ID id);

    void save(T entity);

    void update(T entity);

    void remove(T entity);
}
