package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcDeveloperDAOImpl;
import net.proselyte.pmsystem.model.Developer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static net.proselyte.pmsystem.util.ConnectionUtil.connection;
import static net.proselyte.pmsystem.util.ConnectionUtil.preparedStatement;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link JdbcDeveloperDAOImpl}
 * @author Kyryl Potapenko
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcDeveloperDAOImplTest {
    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmnt;
    @Mock
    ResultSet mockResultSet;
    @Mock
    DataSource mockDataSource;

    Long Id;
    JdbcDeveloperDAOImpl jdbcDeveloperDAO = new JdbcDeveloperDAOImpl();
    Developer developer = new Developer();

    public JdbcDeveloperDAOImplTest() {
        Id = 123L;
        developer.setId(Id);
        developer.setFirstName("Name");
        developer.setLastName("LName");
        developer.setAge(22);
        developer.setSalary(BigDecimal.valueOf(22000));
        developer.setYearsOfExperience(8);
    }

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmnt);
        doNothing().when(mockPreparedStmnt).setLong(anyInt(), anyLong());
        doNothing().when(mockPreparedStmnt).setString(anyInt(), anyString());
        when(mockPreparedStmnt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        connection = mockConn;
        preparedStatement = mockPreparedStmnt;
    }

    @Test
    public void getByIdTest() throws SQLException {
        jdbcDeveloperDAO.getById(Id);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockPreparedStmnt, times(1)).close();
    }
    @Test
    public void saveTest() throws SQLException {
        jdbcDeveloperDAO.save(developer);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).setBigDecimal(anyInt(), BigDecimal.valueOf(anyInt()));
        verify(mockPreparedStmnt, times(3)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
    @Test
    public void updateTest() throws SQLException {
        jdbcDeveloperDAO.update(developer);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(3)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmnt, times(1)).setBigDecimal(anyInt(), BigDecimal.valueOf(anyInt()));
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
    @Test
    public void removeTest() throws SQLException {
        jdbcDeveloperDAO.remove(developer);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
}

