package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcSkillDAOImpl;
import net.proselyte.pmsystem.model.Skill;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Руслан on 22.12.2016.
 */

    @RunWith(MockitoJUnitRunner.class)
    public class JdbcSkillDAOTestDAORemove {
        private Skill skill;
        private JdbcSkillDAOImpl jdbcSkillDAOImpl;
        Long id;

        @Before
        public void setUp() throws Exception {
            skill = mock(Skill.class);
            jdbcSkillDAOImpl = mock(JdbcSkillDAOImpl.class);
        }

        @Test
        public void removeTest() throws Exception {
            stubVoid(jdbcSkillDAOImpl).toReturn().on().remove(skill);
            jdbcSkillDAOImpl.remove(skill);
        }
}
