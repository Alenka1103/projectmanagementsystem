package net.proselyte.pmsystem.model;

/**
 * Simple JavaBean domain object that represents a Skill (Java, SQL, Spring, etc.)
 *
 * @author Eugene Suleimanov
 */
public class Skill extends BaseEntity {

    private String  name;

    public Skill() {

    }
    public Skill(Long id, String name){
        super(id);
        this.name = name;

    }
    public Skill (Long id){
        super(id);
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Skill{" +
                "name='" + name + '\'' +
                '}';
    }
}
