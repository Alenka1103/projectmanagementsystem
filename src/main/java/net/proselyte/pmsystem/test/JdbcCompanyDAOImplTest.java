package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcCompanyDAOImpl;
import net.proselyte.pmsystem.model.Company;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static net.proselyte.pmsystem.util.ConnectionUtil.connection;
import static net.proselyte.pmsystem.util.ConnectionUtil.preparedStatement;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link JdbcCompanyDAOImpl}
 *
 * @author Kyryl Potapenko
 */
@RunWith(MockitoJUnitRunner.class)
public class JdbcCompanyDAOImplTest {

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmnt;
    @Mock
    ResultSet mockResultSet;

    Long Id;
    JdbcCompanyDAOImpl jdbcCompanyDAO = new JdbcCompanyDAOImpl();
    Company Company = new Company();


    public JdbcCompanyDAOImplTest() {
        Id = 123L;
        Company.setId(Id);
        Company.setName("Name");
        Company.setDescription("Desc");
    }

    @Before
    public void setUp() throws SQLException, ClassNotFoundException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmnt);
        doNothing().when(mockPreparedStmnt).setLong(anyInt(), anyLong());
        doNothing().when(mockPreparedStmnt).setString(anyInt(), anyString());
        when(mockPreparedStmnt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        connection = mockConn;
        preparedStatement = mockPreparedStmnt;
    }

    @Test
    public void getByIdTest() throws SQLException, ClassNotFoundException {
        jdbcCompanyDAO.getById(Id);
        
        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockConn, times(0)).commit();
    }

    @Test
    public void saveTest() throws SQLException {
        jdbcCompanyDAO.save(Company);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }

    @Test
    public void updateTest() throws SQLException {
        jdbcCompanyDAO.update(Company);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(2)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }

    @Test
    public void removeTest() throws SQLException {
        jdbcCompanyDAO.remove(Company);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).executeUpdate();
        verify(mockPreparedStmnt, times(1)).close();
    }
}