package src.test.net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcTeamDaoImpl;
import net.proselyte.pmsystem.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Lifyrenko Anton on 12/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class TeamDAOTestDelete {
    private JdbcTeamDaoImpl jdbcTeamDao;
    private Team team;
    Long id;

    @Before
    public void setUp(){
        team = mock(Team.class);
        jdbcTeamDao = mock(JdbcTeamDaoImpl.class);
    }

    @Test
    public void removeTest() throws Exception {
        stubVoid(jdbcTeamDao).toReturn().on().remove(team);
        jdbcTeamDao.remove(team);
    }

}