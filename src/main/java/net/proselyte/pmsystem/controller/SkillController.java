package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcSkillDAOImpl;
import net.proselyte.pmsystem.model.Skill;

/**
 * Controller that handles requests connected with {@link Skill}
 *
 * @author Irina Selutina
 * @author Kyryl Potapenko
 */
public class SkillController {

    JdbcSkillDAOImpl jdbcSkillDAO = new JdbcSkillDAOImpl();

    public Skill getSkillId(long id) {
        return jdbcSkillDAO.getById(id);
    }

    public void saveSkillName(Skill skill) {
        jdbcSkillDAO.save(skill);
    }

    public void updateSkill(Skill skill) {
        jdbcSkillDAO.update(skill);
    }

    public void removeSkill(Skill skill) {
        jdbcSkillDAO.remove(skill);
    }

    public void showAllBase() {jdbcSkillDAO.showAllBase();}
}