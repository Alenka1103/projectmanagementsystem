package src.test.net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcTeamDaoImpl;
import net.proselyte.pmsystem.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Lifyrenko Anton on 12/10/2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class TeamDAOTestSave {
    private JdbcTeamDaoImpl jdbcTeamDaoImpl;
    private Team team;

    @Before
    public void setUp(){
        team = mock(Team.class);
        jdbcTeamDaoImpl = mock(JdbcTeamDaoImpl.class);
    }

    @Test
    public void saveTest() throws Exception {
        stubVoid(jdbcTeamDaoImpl).toReturn().on().save(team);
        jdbcTeamDaoImpl.save(team);
    }

}