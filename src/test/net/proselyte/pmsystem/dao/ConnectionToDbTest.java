package net.proselyte.pmsystem.test;

import org.junit.Test;

import java.sql.*;

/**
 * Test DB connection.
 *
 * @author Kyryl Potapenko
 */
public class ConnectionToDbTest {
    private static String DB_URL = "";
    private static String USER = "";
    private static String PASS = "";

    @Test
    public void readTest() {
        if (!DB_URL.isEmpty()) {
            Statement statement = null;
            Connection connection = null;
            try {
                connection = getConnection(connection);
                boolean reachable = connection.isValid(10);
                System.out.println("Status: " + reachable);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("Goodbye!");
        } else System.out.println("Please fill in connection details");
    }

    public Connection getConnection(Connection connection) {
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            System.out.println("ERROR: Unable to Connect to Database.");
        }
        return connection;
    }
}
