package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Alexey on 12/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerDAOTestGetById {
    private Customer customer;
    private JdbcCustomerDAOImpl jdbcCustomerDAOImpl;
    Long id;

    @Before
    public void setUp() throws Exception {
        customer = mock(Customer.class);
        jdbcCustomerDAOImpl = mock(JdbcCustomerDAOImpl.class);
        id = new Long(customer.getId());
    }

    @Test
    public void getByIdTest() throws Exception {
        when(jdbcCustomerDAOImpl.getById(id)).thenReturn(customer);
        assertEquals(jdbcCustomerDAOImpl.getById(id), customer);
    }

}