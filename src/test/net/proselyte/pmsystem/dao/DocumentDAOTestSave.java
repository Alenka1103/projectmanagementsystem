package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stubVoid;

/**
 * Created by Alexey on 12/10/2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class DocumentDAOTestSave {
    private JdbcDocumentDAOImpl jdbcDocumentDAOImpl;
    private Document document;


    @Before
    public void setUp(){
        document = mock(Document.class);
        jdbcDocumentDAOImpl = mock(JdbcDocumentDAOImpl.class);
    }


    @Test
    public void saveTest() throws Exception {
        stubVoid(jdbcDocumentDAOImpl).toReturn().on().save(document);
        jdbcDocumentDAOImpl.save(document);
    }

}