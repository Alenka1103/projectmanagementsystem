package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcTeamDaoImpl;
import net.proselyte.pmsystem.model.Team;
import net.proselyte.pmsystem.util.ConnectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * @author Anton
 */
public class JdbcTeamDaoImplTest {

    @Before
    public void getConnection() throws SQLException, ClassNotFoundException {
        ConnectionUtil.getConnection();
    }

    @Test
    public void getById() throws Exception {
        Long id = 1l;
        JdbcTeamDaoImpl jdbcTeamDaoImpl = new JdbcTeamDaoImpl();
        Team team = jdbcTeamDaoImpl.getById(id);
        assertTrue(team.getId().equals(id));
    }

    @Test
    public void save() throws Exception {
        Long id = 10050L;
        Team team = new Team();
        team.setId(id);
        team.setName("TestName");
        JdbcTeamDaoImpl jdbcTeamDaoImpl = new JdbcTeamDaoImpl();
        jdbcTeamDaoImpl.save(team);

        Team newTeam = jdbcTeamDaoImpl.getById(id);
        assertTrue(newTeam.getId().equals(team.getId()) && newTeam.getName().equals(team.getName()));
    }

    @Test
    public void update() throws Exception {
        Long id = 1L;
        JdbcTeamDaoImpl jdbcTeamDaoImpl = new JdbcTeamDaoImpl();
        Team team = new Team();
        team.setId(id);
        team.setName("Mr Anderson");
        jdbcTeamDaoImpl.update(team);
        assertTrue(team.getName().equals(jdbcTeamDaoImpl.getById(id).getName()));
    }

    @Test(expected = SQLException.class)
    public void remove() throws Exception {
        Long id = 7l;
        JdbcTeamDaoImpl jdbcTeamDaoImpl = new JdbcTeamDaoImpl();
        Team team = new Team();
        team.setId(id);
        team.setName("Stiven");
        jdbcTeamDaoImpl.save(team);
        jdbcTeamDaoImpl.remove(team);
    }
}