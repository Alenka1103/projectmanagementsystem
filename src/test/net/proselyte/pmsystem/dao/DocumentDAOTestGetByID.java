package net.proselyte.pmsystem.dao;

import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Alexey on 12/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentDAOTestGetByID {
    private JdbcDocumentDAOImpl jdbcDocumentDAOImpl;
    private Document document;
    Long id;

    @Before
    public void setUp(){
        document = mock(Document.class);
        jdbcDocumentDAOImpl = mock(JdbcDocumentDAOImpl.class);
        id = new Long(document.getId());
    }

    @Test
    public void getByIdTest() throws Exception {
        when(jdbcDocumentDAOImpl.getById(id)).thenReturn(document);
        assertEquals(jdbcDocumentDAOImpl.getById(id), document);
    }

}