package net.proselyte.pmsystem.controller;


import net.proselyte.pmsystem.dao.jdbc.JdbcDeveloperDAOImpl;
import net.proselyte.pmsystem.dao.jdbc.JdbcTeamDaoImpl;
import net.proselyte.pmsystem.model.Developer;
import net.proselyte.pmsystem.model.Team;

import java.util.Set;

/**
 * Created by ANTON on 25.12.2016.
 */
public class TeamController {

    JdbcTeamDaoImpl jdbcTeamDao = new JdbcTeamDaoImpl();
    JdbcDeveloperDAOImpl jdbcDeveloperDAO = new JdbcDeveloperDAOImpl();

    Team team = new Team();
    Set<Developer> developers = team.getDevelopers();

    public Team getTeamById(long id) {
        return jdbcTeamDao.getById(id);
    }

    public void saveTeamByName(Team team) {
        jdbcTeamDao.save(team);
    }

    public void updateTeam(Team team) {
        jdbcTeamDao.update(team);
    }

    public void removeTeam(Team team) {
        jdbcTeamDao.remove(team);
    }

    public void showAllTeams(Team team){
        jdbcTeamDao.showAllTeams(team);}

    public void addToTeamList(Developer developer){

        jdbcDeveloperDAO.getById(developer.getId());
        developers.add(developer);
        team.setDevelopers(developers);
    }

    public void printTeamList() {
        for (Developer developer : developers) {
            System.out.println(developer);
        }
    }

}
