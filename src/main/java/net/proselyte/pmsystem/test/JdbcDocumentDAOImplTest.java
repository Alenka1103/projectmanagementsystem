package net.proselyte.pmsystem.test;

import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;
import net.proselyte.pmsystem.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;


/**
 * Tests for {@link JdbcDocumentDAOImpl} class.
 *
 * @author Alexey Volotka
 */
public class JdbcDocumentDAOImplTest {
    public static void main(String[] args) {
        Document document = new Document((long) 4, "4Doc", "4Content");
        JdbcDocumentDAOImpl jdbcDocumentDAO = new JdbcDocumentDAOImpl();
        if (!checkIfIdExist(document.getId())) {
            jdbcDocumentDAO.save(document);
            System.out.println("Data saved successfully");
        } else {
            System.out.println("Enter other id");
        }

        ArrayList<Document> arrayList = (ArrayList<Document>) getAllDocument();
        for (Document document1 : arrayList) {
            System.out.println(document1);
        }

        System.out.println("Test method getById");

        for (long i = 1; checkIfIdExist(i); i++) {
            Document document1 = jdbcDocumentDAO.getById(i);
            System.out.println(document1);
        }

        System.out.println("Test method update");

        Document documentToUpdate = new Document((long) 4, "some other name", "some other content");
        jdbcDocumentDAO.update(documentToUpdate);


        ArrayList<Document> arrayList1 = (ArrayList<Document>) getAllDocument();
        for (long i = 1; checkIfIdExist(i); i++) {
            Document document1 = jdbcDocumentDAO.getById(i);
            System.out.println(document1);
        }

        Document forDelete = new Document((long) 2, "2Doc", "2Content");
        System.out.println("Test delete method ");

        jdbcDocumentDAO.remove(forDelete);

        ArrayList<Document> arrayList2 = (ArrayList<Document>) getAllDocument();
        for (Document document1 : arrayList2) {
            System.out.println(document1);
        }

    }

    private static Collection<Document> getAllDocument() {
        try {
            try (Connection connection = ConnectionUtil.connection) {
                try (Statement statement = connection.createStatement()) {
                    Collection<Document> list = new ArrayList<Document>();
                    try (ResultSet resultSet = statement.executeQuery("SELECT * FROM documents")) {
                        while (resultSet.next()) {
                            Document document = new Document();
                            document.setId(resultSet.getLong(1));
                            document.setName(resultSet.getString(2));
                            document.setContent(resultSet.getString(3));
                            list.add(document);
                        }
                    }
                    return list;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    private static boolean checkIfIdExist(Long id) {
        boolean result = false;
        JdbcDocumentDAOImpl jdbcDocumentDAO = new JdbcDocumentDAOImpl();
        Document document;
        document = jdbcDocumentDAO.getById(id);
        if (document != null) {
            result = true;
        }
        return result;
    }

}