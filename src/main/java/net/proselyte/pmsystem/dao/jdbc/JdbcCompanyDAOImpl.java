package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.CompanyDAO;
import net.proselyte.pmsystem.dao.GenericDAO;
import net.proselyte.pmsystem.model.Company;
import net.proselyte.pmsystem.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static net.proselyte.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link GenericDAO} interface for class {@link Company}.
 *
 * @author Kyryl Potapenko
 */
public class JdbcCompanyDAOImpl implements CompanyDAO {
    private HashSet<String> customers = new HashSet();
    private HashSet<String> projects = new HashSet();
    private HashSet<String> developers = new HashSet();

    public JdbcCompanyDAOImpl() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection failed " + e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Driver not found " + e);
        }
    }

    public Company getById(Long id) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM companies WHERE id = ?");
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createCompany(resultSet);
            } else {
                throw new SQLException("Cannot find Company with id = " + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void save(Company Company) {
        try {
            preparedStatement = connection.prepareStatement("INSERT INTO companies VALUES (?,?,?)");
            preparedStatement.setLong(1, Company.getId());
            preparedStatement.setString(2, Company.getName());
            preparedStatement.setString(3, Company.getDescription());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void update(Company Company) {
        try {
            preparedStatement = connection.prepareStatement("UPDATE companies SET name = ? , description =? where id = ?");
            preparedStatement.setString(1, Company.getName());
            preparedStatement.setString(2, Company.getDescription());
            preparedStatement.setLong(3, Company.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void remove(Company Company) {
        try {
            preparedStatement = connection.prepareStatement("DELETE from companies where id = ?");
            preparedStatement.setLong(1, Company.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    private Company createCompany(ResultSet resultSet) throws SQLException {
        Company Company = new Company();
        Company.setId(resultSet.getLong("id"));
        Company.setName(resultSet.getString("name"));
        Company.setDescription(resultSet.getString("description"));
        return Company;
    }

    public void showRelatedcustomers(Long id) {
        try {
            preparedStatement = connection.prepareStatement
                    ("SELECT customers.name\n" +
                            "FROM customers  JOIN company_customers ON company_customers.customersID = customers.ID\n" +
                            "                 JOIN companies ON company_customers.CompanyID = companies.ID\n" +
                            "                 WHERE companies.ID =?");
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(resultSet.getString(1));
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
        if (!customers.isEmpty()) {
            for (String str : customers) {
                System.out.print(str + "; ");
            }
        } else System.out.println("У компании нет клиентов");
    }

    public void showRelatedprojectsAndDev(Long id) {
        try {
            preparedStatement = connection.prepareStatement
                    ("SELECT projects.NAME,\n" +
                            "       DEVELOPERS.FIRSTNAME\n" +
                            "       FROM projects LEFT JOIN team_developers ON projects.teamId = team_developers.teamId\n" +
                            "                      LEFT JOIN developers ON team_developers.developerId = developers.id\n" +
                            "                      WHERE projects.companyId =?");
            preparedStatement.setLong(1, id);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                projects.add(result.getString(1));
                developers.add(result.getString(2));
            }
            result.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
        if (!projects.isEmpty()) {
            for (String str : projects) {
                System.out.print(str + "; ");
            }
            System.out.println("\nРазработчики компании:");
            for (String str : developers) {
                System.out.print(str + "; ");
            }
        } else System.out.println("У компании нет проектов");
    }

    public Collection<Company> getAll() {
        try {
            try (Connection connection = ConnectionUtil.connection) {
                try (Statement statement = connection.createStatement()) {
                    Collection<Company> list = new ArrayList<Company>();
                    try (ResultSet resultSet = statement.executeQuery("SELECT ID, NAME, DESCRIPTION FROM companies")) {
                        while (resultSet.next()) {
                            Company Company = new Company();
                            Company.setId(resultSet.getLong(1));
                            Company.setName(resultSet.getString(2));
                            Company.setDescription(resultSet.getString(3));
                            list.add(Company);
                        }
                    }
                    return list;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}




