package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.CompanyController;
import net.proselyte.pmsystem.model.Company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Company}
 *
 * @author Kyryl Potapenko
 */
public class CompanyView {

    public void execute() throws IOException {
        Company Company = new Company();
        CompanyController CompanyController = new CompanyController();
        String name;
        Long id;

        writeMessage("* * * Компании * * *" + "\n" +
                "1 - Добавить | 2 - Удалить | 3 - Изменить | 4 - Найти по ID | 5 - Вывести клиентов компании | 6 - Вывести все компании | 7 - Выход в главное меню\n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Укажите название компании:\n");
                name = readString();
                writeMessage("\nУкажите ID компании:\n");
                id = readLong();
                writeMessage("\nКомпания создана!\n");
                Company.setId(id);
                Company.setName(name);
                CompanyController.save(Company);
                System.out.println(CompanyController.getById(id).toString());
                break;
            case 2:
                writeMessage("Укажите ID компании:\n");
                id = readLong();
                CompanyController.remove(CompanyController.getById(id));
                writeMessage("\nКомпания удалена!\n");
                break;
            case 3:
                writeMessage("Укажите ID компании:\n");
                id = readLong();
                writeMessage("Укажите новое название компании:\n");
                name = readString();
                Company.setId(id);
                Company.setName(name);
                CompanyController.update(Company);
                writeMessage("\nИзменения выполнены!\n");
                break;
            case 4:
                writeMessage("Укажите ID компании:\n");
                id = readLong();
                CompanyController.getById(id);
                break;
            case 5:
                writeMessage("Укажите ID компании:\n");
                id = readLong();
                writeMessage("Клиенты компании:");
                CompanyController.showRelatedcustomers(id);
                writeMessage("\nПроекты компании:");
                CompanyController.showRelatedprojectsAndDev(id);
                break;
            case 6:
                writeMessage("Выводим список всех компаний: \n");
                Collection<Company> list = new ArrayList<Company>();
                list = CompanyController.getAll();
                for (Company Company1 : list) {
                    writeMessage(Company1.getId() + ", " + Company1.getName() + ", " + Company1.getDescription() + "\n");
                }
                break;
            case 7:
                writeMessage("Переходим в главное меню...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }
        execute();
    }
}