package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.GenericDAO;
import net.proselyte.pmsystem.dao.TeamDAO;
import net.proselyte.pmsystem.model.Team;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static net.proselyte.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link GenericDAO} interface for class {@link Team}.
 *
 * @author Anton
 */
public class JdbcTeamDaoImpl implements TeamDAO {

    public JdbcTeamDaoImpl() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection to DB failed " + e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Driver not found " + e);
        }
    }

    public void showAllTeams(Team team){
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM TEAMS");
            while (resultSet.next()){
                Long id = resultSet.getLong(1);
                String name = resultSet.getString(2);
                System.out.println(id + "   " + name + "    ");
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public Team getById(Long id) {
        try {
            preparedStatement = connection.prepareStatement("select* from teams where id = ?");
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return createTeam(resultSet);
            } else {
                preparedStatement.close();
                throw new RuntimeException("Cannot find Team with id = " + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void save(Team entity) {
        try {
            preparedStatement = connection.prepareStatement("INSERT INTO teams VALUES (?,?)");
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void update(Team entity) {
        try {
            preparedStatement = connection.prepareStatement("UPDATE teams SET name = ? where id = ?");
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, entity.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        }
    }

    public void remove(Team team) {
        try {
            preparedStatement = connection.prepareStatement("DELETE from teams where id = ?");
            preparedStatement.setLong(1, team.getId());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
        }
    }


    private Team createTeam(ResultSet resultSet) throws SQLException {
        Team team = new Team();
        team.setId(resultSet.getLong(1));
        team.setName(resultSet.getString(2));
        Calendar date = new GregorianCalendar();
        return team;


    }
}