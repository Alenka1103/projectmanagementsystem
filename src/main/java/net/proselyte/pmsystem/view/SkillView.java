package net.proselyte.pmsystem.view;
import net.proselyte.pmsystem.controller.SkillController;
import net.proselyte.pmsystem.model.Skill;

import java.io.IOException;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * @author Created by Iryna on 26.12.16.
 * @author Kyryl Potapenko
 */
public class SkillView {
    SkillController skillController;
    String data;
    Long id;

    public SkillView() {
        skillController = new SkillController();
    }

    public void skillView() throws IOException {
        writeMessage("* * * Skills * * *" + "\n" +
                "1 - ADD | 2 - REMOVE | 3 - ALTER | 4 - FIND BY ID | 5 - SHOW ALL RECORDS| 6 - EXIT IN MAIN MENU\n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter name of skill:\n");
                data = readString();
                writeMessage("\nEnter ID of skill:\n");
                id = readLong();
                saveSkill(id, data);
                break;
            case 2:
                writeMessage("Enter Skill ID:\n");
                id = readLong();
                removeSkill(id);
                break;
            case 3:
                writeMessage("Enter Skill ID:\n");
                id = readLong();
                writeMessage("Enter new Skill name:\n");
                data = readString();
                updateSkill(id, data);
                break;
            case 4:
                writeMessage("Enter Skill ID:\n");
                id = readLong();
                getSkillId(id);
                break;
            case 5:
                skillController.showAllBase();
                break;
            case 6:
                writeMessage("We're about to main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }
        skillView();
    }


    public void getSkillId(long id){
        Skill skill = skillController.getSkillId(id);
        System.out.println(skill);
    }

    public void saveSkill(Long id, String name) {
        Skill skill = new Skill();
        skill.setName(name);
        skill.setId(id);
        skillController.saveSkillName(skill);
        System.out.println("The skill" + skill.getName() + ", ID " + skill.getId() + ", is successfully added");
    }

    public void updateSkill(Long id, String newName) {
        Skill skill = skillController.getSkillId(id);
        skill.setName(newName);
        skillController.updateSkill(skill);
        System.out.println("The skill" + skill.getName() + ", ID " + skill.getId() + ", is successfully updated");
    }

    public void removeSkill(Long id) {
        Skill skill = skillController.getSkillId(id);
        skillController.removeSkill(skill);
        System.out.println("The skill" + skill.getName() + ", ID " + skill.getId() + ", is successfully removed");
    }
}