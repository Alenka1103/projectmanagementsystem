package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.TeamController;
import net.proselyte.pmsystem.model.Team;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class that contains method to output data connected with {@link Team}.
 */
public class TeamView {
    public void choice() throws IOException {
        Team team = new Team();
        Long id;
        String name;

        TeamController teamController = new TeamController();

        writeMessage("\n 1 - Add team \n 2 - Delete team \n 3 - Change team \n 4 - Find team by ID \n 5 - Show all teams \n 6 - Exit to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber){
            case 1:
                writeMessage("\n Enter team name");
                name = readString();
                writeMessage("\n Enter team Id");
                id = readLong();
                writeMessage("\n Team created");

                team.setId(id);
                team.setName(name);
                teamController.saveTeamByName(team);
                choice();
                break;

            case 2:
                writeMessage("\n Enter team Id");
                id = readLong();
                teamController.removeTeam(teamController.getTeamById(id));
                writeMessage("\n Team deleted \n");
                choice();
                break;

            case 3:
                writeMessage("\n Enter team Id");
                id = readLong();
                writeMessage("\n Enter new team name");
                name = readString();

                team.setId(id);
                team.setName(name);

                teamController.updateTeam(team);
                System.out.println("\n Team changed \n");
                choice();
                break;

            case 4:
                writeMessage("\n Enter team Id");
                id = readLong();
                teamController.getTeamById(id);
                System.out.println(teamController.getTeamById(id).toString());
                choice();
                break;

            case 5:writeMessage("All teams:");
                teamController.showAllTeams(team);
                choice();
                break;

            case 6:
                writeMessage("Exiting to main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;

                default:
                break;

        }
        choice();

    }

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage (String message){
        System.out.println(message);
    }

    public static String readString() throws IOException{
        return bufferedReader.readLine();
    }

    public static Integer readInt()throws IOException{
        int number = 0;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e){
            writeMessage("Data typed incorrectly. Repeat enter");
            readInt();
        }
        return number;
    }

    public static Long readLong() throws IOException{
        long number = 0;
        try {
            number = Long.parseLong(bufferedReader.readLine());
        } catch (NumberFormatException e){
            writeMessage("Data typed incorrectly. Repeat enter");
            readLong();
        }
        return number;
    }

}
