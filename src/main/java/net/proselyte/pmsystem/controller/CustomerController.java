package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.jdbc.JdbcCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;

import java.util.ArrayList;

/**
 * Created by Руслан on 18.01.2017.
 */
public class CustomerController {
    JdbcCustomerDAOImpl jdbcCustomerDAO = new JdbcCustomerDAOImpl();

    public Customer getById(Long customerId) {
        return jdbcCustomerDAO.getById(customerId);
    }

    public void save(Customer customer) {
        jdbcCustomerDAO.save(customer);
    }

    public void update(Customer customer) {
        jdbcCustomerDAO.update(customer);
    }

    public void remove(Customer customer) {
        jdbcCustomerDAO.remove(customer);
    }

    public ArrayList<Customer> showCustomer(Customer customer) {
        return jdbcCustomerDAO.showCustomer(customer);
    }

}
